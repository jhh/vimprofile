"================================================================================
"================================================================================
"================================================================================
set nocompatible              " Be iMproved, required.
filetype off                  " Required.

" Set the runtime path to include Vundle and initialize.
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" Alternatively, pass a path where Vundle should install plugins
" call vundle#begin('~/some/path/here')

" Let Vundle manage Vundle, required.
Plugin 'gmarik/Vundle.vim'

" Third party plugins (add optional stuff below these).
Plugin 'diffchanges.vim' " View difference between open file and file on disk.
Plugin 'bronson/vim-trailing-whitespace' " Remove whitespaces.
Plugin 'ervandew/supertab' " Autocompletes words with tab key.
Plugin 'kien/ctrlp.vim' " Easy search filenames.
Plugin 'ZoomWin' " Toggle a window 'full screen'.
Plugin 'scrooloose/syntastic' " Checks syntax on file save.
Plugin 'matze/vim-move' " Move marked lines up and down.
Plugin 'vim-airline/vim-airline' " Visual statusline.
Plugin 'vim-airline/vim-airline-themes' " Airline themes.
Plugin 'tpope/vim-endwise' " Auto add 'end' after typing def .. enter.
Plugin 'scrooloose/nerdcommenter' " Makes comment, uncomment code easy.
Plugin 'danchoi/ri.vim' " Read ri documentation. Depends: $ rvm docs generate-ri
Plugin 'majutsushi/tagbar' " Navigate methods etc.
Plugin 'tomasr/molokai' " TextMate inspired color scheme.
Plugin 'nanotech/jellybeans.vim' " A theme with good support for terminal.
Plugin 'noahfrederick/vim-hemisu' " A color scheme with dark and light.
Plugin 'mileszs/ack.vim' " Make ack command available for local searching.
Plugin 'Yggdroot/indentLine' " Make indents visually easier to follow.
Plugin 'Gundo' " Visualize and navigate the Vim undo tree.
Plugin 'mattn/emmet-vim' " Speed up writing html.
" Plugin 'chase/vim-ansible-yaml' " Fix indention and syntax highligthing for Ansible YAML.
Plugin 'pearofducks/ansible-vim' " Replacement for vim-ansible-yaml, has support for hosts file as well.
Plugin 'joonty/vdebug' " Multi-language DBGP debugger, PHP, Python.. *
Plugin 'hynek/vim-python-pep8-indent' " Improve (fix?) Python indention on newlines.
Plugin 'vim-scripts/nginx.vim' " Nginx syntax highlight.

" * Local options for vdebug should be placed in ~/.vimrc.after

" All of your Plugins must be added before the following line.
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" See :h vundle for more details or wiki for FAQ.
" Put your non-Plugin stuff after this line.
"================================================================================
"================================================================================
"================================================================================

"
" Some sane defaults for indents.
" See also ftplugin/ruby.vim file for Ruby settings.
set tabstop=4
set shiftwidth=4
"
" Replace indends with spaces.
set expandtab
"
" A scratch pad appears when using autocompletion, this gets anoying.
" Disabling it.
set completeopt-=preview
"
" Display line numbers.
set number
"
" Copy indent from current line when starting a new line.
set autoindent
"
" Show parts of the text in another font or color.
syntax enable
"
" Character encoding for files.
set encoding=utf-8
"
" Make mouse handling in terminal behave more like gVim.
set mouse=a
"
" Default to softwrap long lines.
set wrap
"
" Highlight searches.
set hlsearch
"
" Incremental search (display match while typing).
set incsearch
"
" Instead of open split view to the top, open it below.
set splitbelow
"
" Instead of open vertical split view to the left, open it to the right.
set splitright
"
" Always show status bar.
set laststatus=2
"
" Reload file if :checktime has been triggered, and file has not been
" changed inside of Vim.
set autoread
"
" A nice way to navigate command line options with tab.
" http://vim.wikia.com/wiki/Great_wildmode/wildmenu_and_console_mouse
set wildmenu
set wildmode=list:longest,full
"
" Navigate between if, end.. etc. with %.
runtime macros/matchit.vim
"
" Change modifier key for move plugin.
" https://github.com/matze/vim-move
let g:move_key_modifier = 'C'
"
" Set <Leader> (see :h mapleader) key to "ø" key.
" Convienient and easy to reach on Norwegian keyboards.
let mapleader = "ø"
"
" Enable search inside tags file (require ctags to generate).
let g:ctrlp_extensions = ['tag']
"
" Disable ctrlp caching, avoids cache out of sync.
let g:ctrlp_use_caching = 0
"
" IndentLine disabled by default (toggle with key combo).
let g:indentLine_enabled = 0
"
" Toggle IndentLines.
nnoremap <Leader>i :IndentLinesToggle<CR>
"
" Map colon to space, save tons of shift+period typing.
map <space> :
"
" Highlight variables, without jumping around like # or *.
nnoremap <silent> <expr> æ ToggleWordHlsearch()
"
" Quick access to :Explore navigation.
nnoremap <Leader>e :Explore <CR>
"
" Alternative access to (often used) exit window command.
nnoremap <Leader>q :q
"
" Alternative access to (often used) save file command.
" Don't add <CR>, in case key is pressed by accident.
nnoremap <Leader>w :w
"
" Alternative access to (often used) split window command.
nnoremap <Leader>s :split <CR>
"
" Alternative access to (often used) vsplit window command.
nnoremap <Leader>v :vsplit <CR>
"
" Alternative access to (often used) next below/right window command.
" (Upper left corner, below esc key on Norwegian keyboards.)
nnoremap <Bar> :wincmd w <CR>
"
" Alternative access to (often used) next above/left window command.
" (shift+bar on Norwegian keyboards.)
nnoremap § :wincmd W <CR>
"
" Toggle difference between open file and file on disk.
nnoremap <Leader>d :DiffChangesDiffToggle <CR>
"
" Check if files are changed on disk and need reloading.
" Use :DiffChangesDiffToggle to see difference between
" your open file and the file on disk.
nnoremap <Leader><F5> :checktime <CR>
"
" Toggle ZoomWin.
nnoremap <Leader>z :ZoomWin<CR>
"
" Toggle tagbar.
nnoremap <Leader>T :TagbarToggle<CR>
"
" Go to definition, tag.
nnoremap å <C-]>
"
" Show list of available tags for word under coursor.
nnoremap Å g]
"
" Update tags.
nnoremap <Leader>å :call UpdateTagsFile() <CR>
"
" Need a new tab, you got it.
nnoremap <Leader>t :tabedit <CR>
"
" ctrl-c instead of typing ESC or ctrl-[ to leave insert mode etc.
" http://stackoverflow.com/q/80677
inoremap <C-c> <Esc><Esc>
"
" Map Gundo toggle.
nnoremap <Leader>u :GundoToggle<CR>
"
" CtrlP default to current directory (see ":h pwd" and ":h cd").
let g:ctrlp_working_path_mode = ''
"
" Configuration of tool that search the current directory (:pwd) for string in
" files.
"
" If 'The Silver Searcher' ag binary is available, it'll be used.
" Otherwise ack 1.96 perl script will be used as fallback.
" Ack 1.x and 2.x have different interface so the 1.96 script is provided
" for consistency.
if executable('ag')
    let g:ack_default_options = ''
    let g:ackprg = 'ag --nogroup --nocolor --column'
    nnoremap <Leader>g :Ack<SPACE><C-R><C-W>
    " Widen search to include all filetypes (except hidden and ignored).
    nnoremap <Leader>G :Ack<SPACE>-a<SPACE><C-R><C-W>
elseif executable('perl') && filereadable(expand("~/.vim/utils/ack-1.96-single-file"))
    let g:ack_default_options = ''
    let g:ackprg = "perl ". expand("~/.vim/utils/ack-1.96-single-file") . " --nocolor --nogroup --column"
    nnoremap <Leader>g :Ack<SPACE><C-R><C-W>
    " Widen search to include all filetypes (except hidden and ignored).
    nnoremap <Leader>G :Ack<SPACE>-a<SPACE><C-R><C-W>
else
    nnoremap <Leader>g :echo "Can't do that without 'ack'."<CR>
    nmap <Leader>G <Leader>g
endif
"
" Script section.
" ================================================================================
"
" Update ctags IF "tags" file exists in current directory (:h pwd).
" To create a new ctags file, just do "$ touch tags" in the root folder of
" your project and the tags file should be updated when calling method.
" Use ":cd <project-root>" if your :pwd is in wrong directory first.
function! UpdateTagsFile()
    if executable('ruby')
        let cmd = 'ruby ~/.vim/utils/update_tags_file.rb --update-tags'
        let cmd = cmd . ' --working-directory ' . shellescape(getcwd())
        execute "!" . cmd
    else
        echo "Ruby executable not found in path!"
    end
endfunction
"
" Only do this part when compiled with support for autocommands.
if has("autocmd")
    "
    " Remember last position when reopening a file.
    " /usr/share/vim/vim74/vimrc_example.vi
    "
    " When editing a file, always jump to the last known cursor position.
    " Don't do it when the position is invalid or when inside an event handler
    " (happens when dropping a file on gvim).
    " Also don't do it when the mark is in the first line, that is the default
    " position when opening a file.
    autocmd BufReadPost *
                \ if line("'\"") > 1 && line("'\"") <= line("$") |
                \   execute "normal! g`\"" |
                \ endif
    "
    " Check if file(s) are updated on disk when cursor stop
    " blinking in normal and insert mode, or after entering another window.
    " You can see current timeout with ":set updatetime?".
    " http://stackoverflow.com/a/927634
    autocmd CursorHold,CursorHoldI,WinEnter * checktime
    "
    " Sample SuperTab configuration from [:help supertab-completionchaining].
    " Attempt omni completion, falls back to keyword completion if not found.
    " (Uses <c-n> instead of <c-p> as of personal preference.)
    autocmd FileType *
                \ if &omnifunc != '' |
                \   call SuperTabChain(&omnifunc, "<c-n>") |
                \   call SuperTabSetDefaultCompletionType("<c-x><c-u>") |
                \ endif
endif " has("autocmd")
"
" Toggle highlighted word searches.
" http://superuser.com/a/255120
" http://vim.wikia.com/wiki/Highlight_all_search_pattern_matches
function! ToggleWordHlsearch()
    if @/ =~ '^\\<'.expand('<cword>').'\\>$'
        let @/ = ""
        return ":silent nohlsearch\<CR>"
    endif
    let @/ = '\<'.expand('<cword>').'\>'
    return ":silent set hlsearch\<CR>"
endfunction
"
" Method for toggling between color schemes.
" Pass parameter 1 for updating airline theme.
" If called during start up, don't pass parameter 1. AirlineTheme can not be
" refreshed during start up (it's to early).
function! ToggleColorScheme(...)
    "
    " Initial values.
    let updateairline=0
    if a:0 == 1
        let updateairline=a:1
    endif
    if !exists('g:togglecolorscheme')
        let g:togglecolorscheme=0
    endif
    color default
    colorscheme default
    if exists('g:indentLine_color_term')
        unlet g:indentLine_color_term
    endif
    "
    " Toggle colorschemes.
    if g:togglecolorscheme == 0
        let g:togglecolorscheme=1
        color molokai
        let g:molokai_original = 1
        if !has('gui_running')
            let g:rehash256 = 1
        endif
        let g:airline_theme = 'molokai'
    elseif g:togglecolorscheme == 1
        let g:togglecolorscheme=2
        let g:indentLine_color_term = 239
        color jellybeans
        let g:airline_theme = 'jellybeans'
    elseif g:togglecolorscheme == 2
        let g:togglecolorscheme=3
        let g:indentLine_color_term = 239
        colorscheme hemisu
        set background=dark
        let g:airline_theme = 'dark'
    elseif g:togglecolorscheme == 3
        let g:togglecolorscheme=0
        colorscheme hemisu
        set background=light
        let g:airline_theme = 'light'
    endif
    "
    " Update airline theme.
    if updateairline == 1
        :AirlineRefresh
    endif
endfunction
"
" Read '~/.vimrc.after' if exists.
" Use this to add settings only for local machine.
" Add 'g:togglecolorscheme = {0-3}' to override default colorscheme.
"
" For example:
" let g:togglecolorscheme = 3
"
if filereadable(expand("~/.vimrc.after"))
    execute "so " . expand("~/.vimrc.after")
endif
"
" Nice colors, if available.
if $TERM == "xterm-256color" || $TERM == "screen-256color" || has('gui_running')
    "
    " Enable 256 colors for terminals.
    set t_Co=256
    "
    " Pretty color scheme.
    :silent! exec ToggleColorScheme()
    "
    " Map toggle through color schemes to a key.
    nnoremap <Leader><F3> :exec ToggleColorScheme(1) <CR>
else
    "
    " Toggle off Airline, and use default instead.
    if has("autocmd")
        autocmd VimEnter * :AirlineToggle
    endif
    nnoremap <Leader><F3> :echo "I whish for 256 colors to make color scheme look pretty.."<CR>
endif

"
" Misc shortcut commands.
"
" Pretty balancing of json file.
com! FormatJSON %!python -m json.tool
