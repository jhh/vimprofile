"
" Settings for Yaml files (*.yml).
"================================================================================

"
" 2 spaces for tabs seems better.
setlocal expandtab
setlocal tabstop=2
setlocal shiftwidth=2
