"
" Settings for Python files (*.py).
"================================================================================

setlocal expandtab
setlocal colorcolumn=80

"
" Recommendations from:
" https://code.djangoproject.com/wiki/UsingVimWithDjango
setlocal shiftwidth=4
setlocal tabstop=4
setlocal softtabstop=4
