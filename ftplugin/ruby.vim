"
" Settings for Ruby files (*.rb).
"================================================================================

"
" By convention, Ruby files are expected to have 2 spaces for tabs.
setlocal expandtab
setlocal tabstop=2
setlocal shiftwidth=2
