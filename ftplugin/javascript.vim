"
" Settings for JavaScript files (*.js).
"================================================================================

"
" At least by Drupal convention, JS files are expected to have 2 spaces for tabs.
setlocal expandtab
setlocal tabstop=2
setlocal shiftwidth=2
