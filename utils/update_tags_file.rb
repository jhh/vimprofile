require 'optparse'
require 'shellwords'

def parse_args
  options = {}
  parser = OptionParser.new do |opts|
    opts.banner = "Helper script that updates (c)tags file\n"
    opts.banner += "Looks for \"tags\" file in your *current directory* and it'll updates accordingly\n"
    opts.banner += "Usage: ruby #{File.basename(__FILE__)} [options]"
    opts.on("-u", "--update-tags", "Update ctags file found in current directory") do |o|
      options[:update_tags] = o
    end
    opts.on("-d", "--dry-run", "Don't apply any destructive changes, print actions instead") do |o|
      options[:dry_run] = o
    end
    opts.on("-wMANDATORY", "--working-directory MANDATORY", "Apply a working directory where \"tags\" file exists. Defaults to \".\".") do |o|
      options[:working_directory] = o
    end
    opts.on_tail("-h", "--help", "This message") do |o|
      puts opts
      exit
    end
  end
  parser.parse!
  # Check for mandetary option or exit.
  if !options[:update_tags]
    puts "Expected [-u|--update-tags], exiting.."
    puts ""
    puts parser
    exit
  end
  options
end

def run(shell_command, args_options)
  if args_options[:dry_run]
    puts shell_command
  elsif args_options[:update_tags]
    puts "> " + shell_command
    system shell_command
  end
end

def file_age_seconds(file_path)
    (Time.now - File.mtime(file_path)).to_i
end

def additional_paths(args_options)
  result = ""
  if File.exists?("Gemfile") && `which bundle`.length > 0
    require 'rubygems'
    require 'bundler'
    run("bundle check || bundle", args_options)
    paths = Bundler.load.specs.map(&:full_gem_path).map{|g| Shellwords.escape(g)}
    result = "#{paths.join(' ')}"
  end
  result
end

def execute(args_options, working_directory, tags_file_path)
  if File.exists?(tags_file_path) && `which ctags`.length > 0
    Dir.chdir(working_directory) do
      age = file_age_seconds tags_file_path
      # Recreate if older than an hour, otherwise just add new tags, unless file is zero of course.
      if age > (60 * 60) || File.zero?(tags_file_path)
        create_tags_cmd = "ctags -R . #{additional_paths(args_options)}"
      else
        create_tags_cmd = "ctags -R -a ."
      end
      run("#{create_tags_cmd}", args_options)
    end
  else
    puts "Nothing to do."
    puts "Tags file do not exists in working directory, or ctags not installed."
  end
end

args_options = parse_args
working_directory = File.expand_path(args_options[:working_directory] || ".")
tags_file_path = File.expand_path("tags", working_directory)

#
# Run!
execute args_options, working_directory, tags_file_path

