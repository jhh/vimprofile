#!/bin/bash

set -e

func_get_repo() {
    local src="$1"
    local dst="$2"
    if [ ! -d $dst ]; then
        git clone "$src" "$dst"
    else
        cd "$dst"
        git pull origin
        cd -
    fi
}

func_set_sym() {
    local src="$1"
    local target="$2"
    if [ -L $target ]; then
        rm "$target"
    fi
    if [ -e $target ]; then
        echo "Target (${target}) exists and is not a symbolic link."
        exit 22
    fi
    ln -s "$src" "$target"
}

func_get_repo \
    "https://github.com/gmarik/Vundle.vim.git" \
    "$HOME/.vim/bundle/Vundle.vim"

func_get_repo \
    "https://bitbucket.org/jhh/vimprofile.git" \
    "$HOME/git/vimprofile"

func_set_sym "$HOME/git/vimprofile/vimrc" "$HOME/.vimrc"
func_set_sym "$HOME/git/vimprofile/ftplugin" "$HOME/.vim/ftplugin"
func_set_sym "$HOME/git/vimprofile/utils" "$HOME/.vim/utils"

if [ -t 0 ]; then
    echo "Configuring Vim from a terminal."
    vim +PluginInstall +qall
else
    echo "Configuring Vim from stdin."
    vim - +PluginInstall +qall
fi
